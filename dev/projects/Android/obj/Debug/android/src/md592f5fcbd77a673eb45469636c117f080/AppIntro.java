package md592f5fcbd77a673eb45469636c117f080;


public abstract class AppIntro
	extends md592f5fcbd77a673eb45469636c117f080.AppIntroBase
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("AppIntro.AppIntro, AppIntro", AppIntro.class, __md_methods);
	}


	public AppIntro ()
	{
		super ();
		if (getClass () == AppIntro.class)
			mono.android.TypeManager.Activate ("AppIntro.AppIntro, AppIntro", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
