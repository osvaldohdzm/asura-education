package md592f5fcbd77a673eb45469636c117f080;


public class AppIntroFragment
	extends md592f5fcbd77a673eb45469636c117f080.AppIntroBaseFragment
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("AppIntro.AppIntroFragment, AppIntro", AppIntroFragment.class, __md_methods);
	}


	public AppIntroFragment ()
	{
		super ();
		if (getClass () == AppIntroFragment.class)
			mono.android.TypeManager.Activate ("AppIntro.AppIntroFragment, AppIntro", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
