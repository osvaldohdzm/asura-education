using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;
using Java.Interop;
using Java.Lang;

namespace AsuraEducationPlatform.Droid.Libraries
{
    public enum IndicatorStyle
    {
        None = 0,
        Triangle = 1,
        Underline = 2
    }

    public class TitlePageIndicator : View, PageIndicator
    {
        /**
	     * Percentage indicating what percentage of the screen width away from
	     * center should the underline be fully faded. A value of 0.25 means that
	     * halfway between the center of the screen and an edge.
	     */
        private const float SELECTION_FADE_PERCENTAGE = 0.25f;

        /**
	     * Percentage indicating what percentage of the screen width away from
	     * center should the selected text bold turn off. A value of 0.05 means
	     * that 10% between the center and an edge.
	     */
        private const float BOLD_FADE_PERCENTAGE = 0.05f;
        private const int INVALID_POINTER = -1;
        private readonly float mFooterIndicatorUnderlinePadding;
        private readonly int mTouchSlop;
        private int mActivePointerId = INVALID_POINTER;
        private bool mBoldText;

        private IOnCenterItemClickListener mCenterItemClickListener;

        /** Left and right side padding for not active view titles. */
        private float mClipPadding;
        private Color mColorSelected;
        private Color mColorText;
        private int mCurrentOffset;
        private int mCurrentPage;
        private float mFooterIndicatorHeight;
        private IndicatorStyle mFooterIndicatorStyle;
        private float mFooterLineHeight;
        private float mFooterPadding;
        private bool mIsDragging;
        private float mLastMotionX = -1;
        private ViewPager.IOnPageChangeListener mListener;
        private readonly Paint mPaintFooterIndicator = new Paint();
        private readonly Paint mPaintFooterLine = new Paint();
        private readonly Paint mPaintText = new Paint();
        private Path mPath;
        private int mScrollState;
        private float mTitlePadding;
        private TitleProvider mTitleProvider;
        private float mTopPadding;

        private ViewPager mViewPager;

        public TitlePageIndicator(Context context) : this(context, null)
        {
        }

        public TitlePageIndicator(Context context, IAttributeSet attrs) : this(context, attrs,
            Resource.Attribute.vpiTitlePageIndicatorStyle)
        {
        }

        public TitlePageIndicator(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            //Load defaults from resources
            var res = Resources;
#pragma warning disable CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            int defaultFooterColor = res.GetColor(Resource.Color.default_title_indicator_footer_color);
#pragma warning restore CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            var defaultFooterLineHeight =
                res.GetDimension(Resource.Dimension.default_title_indicator_footer_line_height);
            var defaultFooterIndicatorStyle =
                res.GetInteger(Resource.Integer.default_title_indicator_footer_indicator_style);
            var defaultFooterIndicatorHeight =
                res.GetDimension(Resource.Dimension.default_title_indicator_footer_indicator_height);
            var defaultFooterIndicatorUnderlinePadding =
                res.GetDimension(Resource.Dimension.default_title_indicator_footer_indicator_underline_padding);
            var defaultFooterPadding = res.GetDimension(Resource.Dimension.default_title_indicator_footer_padding);
#pragma warning disable CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            int defaultSelectedColor = res.GetColor(Resource.Color.default_title_indicator_selected_color);
#pragma warning restore CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            var defaultSelectedBold = res.GetBoolean(Resource.Boolean.default_title_indicator_selected_bold);
#pragma warning disable CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            int defaultTextColor = res.GetColor(Resource.Color.default_title_indicator_text_color);
#pragma warning restore CS0618 // 'Resources.GetColor(int)' est� obsoleto: 'deprecated'
            var defaultTextSize = res.GetDimension(Resource.Dimension.default_title_indicator_text_size);
            var defaultTitlePadding = res.GetDimension(Resource.Dimension.default_title_indicator_title_padding);
            var defaultClipPadding = res.GetDimension(Resource.Dimension.default_title_indicator_clip_padding);
            var defaultTopPadding = res.GetDimension(Resource.Dimension.default_title_indicator_top_padding);

            //Retrieve styles attributes
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.TitlePageIndicator, defStyle,
                Resource.Style.Widget_TitlePageIndicator);

            //Retrieve the colors to be used for this view and apply them.
            mFooterLineHeight = a.GetDimension(Resource.Styleable.TitlePageIndicator_footerLineHeight,
                defaultFooterLineHeight);
            mFooterIndicatorStyle =
                (IndicatorStyle) a.GetInteger(Resource.Styleable.TitlePageIndicator_footerIndicatorStyle,
                    defaultFooterIndicatorStyle);
            mFooterIndicatorHeight = a.GetDimension(Resource.Styleable.TitlePageIndicator_footerIndicatorHeight,
                defaultFooterIndicatorHeight);
            mFooterIndicatorUnderlinePadding =
                a.GetDimension(Resource.Styleable.TitlePageIndicator_footerIndicatorUnderlinePadding,
                    defaultFooterIndicatorUnderlinePadding);
            mFooterPadding = a.GetDimension(Resource.Styleable.TitlePageIndicator_footerPadding, defaultFooterPadding);
            mTopPadding = a.GetDimension(Resource.Styleable.TitlePageIndicator_topPadding, defaultTopPadding);
            mTitlePadding = a.GetDimension(Resource.Styleable.TitlePageIndicator_titlePadding, defaultTitlePadding);
            mClipPadding = a.GetDimension(Resource.Styleable.TitlePageIndicator_clipPadding, defaultClipPadding);
            mColorSelected = a.GetColor(Resource.Styleable.TitlePageIndicator_selectedColor, defaultSelectedColor);
            mColorText = a.GetColor(Resource.Styleable.TitlePageIndicator_textColor, defaultTextColor);
            mBoldText = a.GetBoolean(Resource.Styleable.TitlePageIndicator_selectedBold, defaultSelectedBold);

            var textSize = a.GetDimension(Resource.Styleable.TitlePageIndicator_textSize, defaultTextSize);
            var footerColor = a.GetColor(Resource.Styleable.TitlePageIndicator_footerColor, defaultFooterColor);
            mPaintText.TextSize = textSize;
            mPaintText.AntiAlias = true;
            mPaintFooterLine.SetStyle(Paint.Style.FillAndStroke);
            mPaintFooterLine.StrokeWidth = mFooterLineHeight;
            mPaintFooterLine.Color = footerColor;
            mPaintFooterIndicator.SetStyle(Paint.Style.FillAndStroke);
            mPaintFooterIndicator.Color = footerColor;

            a.Recycle();

            var configuration = ViewConfiguration.Get(context);
#pragma warning disable CS0618 // 'ViewConfigurationCompat' est� obsoleto: 'This class is obsoleted in this android platform'
#pragma warning disable CS0618 // 'ViewConfigurationCompat.GetScaledPagingTouchSlop(ViewConfiguration)' est� obsoleto: 'deprecated'
            mTouchSlop = ViewConfigurationCompat.GetScaledPagingTouchSlop(configuration);
#pragma warning restore CS0618 // 'ViewConfigurationCompat.GetScaledPagingTouchSlop(ViewConfiguration)' est� obsoleto: 'deprecated'
#pragma warning restore CS0618 // 'ViewConfigurationCompat' est� obsoleto: 'This class is obsoleted in this android platform'
        }

        public void SetViewPager(ViewPager view)
        {
            var adapter = view.Adapter;
            if (adapter == null) throw new IllegalStateException("ViewPager does not have adapter instance.");
            if (!(adapter is TitleProvider))
                throw new IllegalStateException(
                    "ViewPager adapter must implement TitleProvider to be used with TitlePageIndicator.");
            mViewPager = view;
#pragma warning disable CS0618 // 'ViewPager.SetOnPageChangeListener(ViewPager.IOnPageChangeListener)' est� obsoleto: 'deprecated'
            mViewPager.SetOnPageChangeListener(this);
#pragma warning restore CS0618 // 'ViewPager.SetOnPageChangeListener(ViewPager.IOnPageChangeListener)' est� obsoleto: 'deprecated'
            mTitleProvider = (TitleProvider) adapter;
            Invalidate();
        }

        public void SetViewPager(ViewPager view, int initialPosition)
        {
            SetViewPager(view);
            SetCurrentItem(initialPosition);
        }

        public void NotifyDataSetChanged()
        {
            Invalidate();
        }

        public void SetCurrentItem(int item)
        {
            if (mViewPager == null) throw new IllegalStateException("ViewPager has not been bound.");
            mViewPager.CurrentItem = item;
            mCurrentPage = item;
            Invalidate();
        }

        public void OnPageScrollStateChanged(int state)
        {
            mScrollState = state;

            if (mListener != null) mListener.OnPageScrollStateChanged(state);
        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
            mCurrentPage = position;
            mCurrentOffset = positionOffsetPixels;
            Invalidate();

            if (mListener != null) mListener.OnPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        public void OnPageSelected(int position)
        {
            if (mScrollState == ViewPager.ScrollStateIdle)
            {
                mCurrentPage = position;
                Invalidate();
            }

            if (mListener != null) mListener.OnPageSelected(position);
        }

        public void SetOnPageChangeListener(ViewPager.IOnPageChangeListener listener)
        {
            mListener = listener;
        }

        public int GetFooterColor()
        {
            return mPaintFooterLine.Color;
        }

        public void SetFooterColor(Color footerColor)
        {
            mPaintFooterLine.Color = footerColor;
            mPaintFooterIndicator.Color = footerColor;
            Invalidate();
        }

        public float GetFooterLineHeight()
        {
            return mFooterLineHeight;
        }

        public void SetFooterLineHeight(float footerLineHeight)
        {
            mFooterLineHeight = footerLineHeight;
            mPaintFooterLine.StrokeWidth = mFooterLineHeight;
            Invalidate();
        }

        public float GetFooterIndicatorHeight()
        {
            return mFooterIndicatorHeight;
        }

        public void SetFooterIndicatorHeight(float footerTriangleHeight)
        {
            mFooterIndicatorHeight = footerTriangleHeight;
            Invalidate();
        }

        public float GetFooterIndicatorPadding()
        {
            return mFooterPadding;
        }

        public void SetFooterIndicatorPadding(float footerIndicatorPadding)
        {
            mFooterPadding = footerIndicatorPadding;
            Invalidate();
        }

        public IndicatorStyle GetFooterIndicatorStyle()
        {
            return mFooterIndicatorStyle;
        }

        public void SetFooterIndicatorStyle(IndicatorStyle indicatorStyle)
        {
            mFooterIndicatorStyle = indicatorStyle;
            Invalidate();
        }

        public Color GetSelectedColor()
        {
            return mColorSelected;
        }

        public void SetSelectedColor(Color selectedColor)
        {
            mColorSelected = selectedColor;
            Invalidate();
        }

        public bool IsSelectedBold()
        {
            return mBoldText;
        }

        public void SetSelectedBold(bool selectedBold)
        {
            mBoldText = selectedBold;
            Invalidate();
        }

        public int GetTextColor()
        {
            return mColorText;
        }

        public void SetTextColor(Color textColor)
        {
            mPaintText.Color = textColor;
            mColorText = textColor;
            Invalidate();
        }

        public float GetTextSize()
        {
            return mPaintText.TextSize;
        }

        public void SetTextSize(float textSize)
        {
            mPaintText.TextSize = textSize;
            Invalidate();
        }

        public float GetTitlePadding()
        {
            return mTitlePadding;
        }

        public void SetTitlePadding(float titlePadding)
        {
            mTitlePadding = titlePadding;
            Invalidate();
        }

        public float GetTopPadding()
        {
            return mTopPadding;
        }

        public void SetTopPadding(float topPadding)
        {
            mTopPadding = topPadding;
            Invalidate();
        }

        public float GetClipPadding()
        {
            return mClipPadding;
        }

        public void SetClipPadding(float clipPadding)
        {
            mClipPadding = clipPadding;
            Invalidate();
        }

        public void SetTypeface(Typeface typeface)
        {
            mPaintText.SetTypeface(typeface);
            Invalidate();
        }

        public Typeface GetTypeface()
        {
            return mPaintText.Typeface;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            if (mViewPager == null) return;
            var count = mViewPager.Adapter.Count;
            if (count == 0) return;

            //Calculate views bounds
            var bounds = CalculateAllBounds(mPaintText);
            var boundsSize = bounds.Count;

            //Make sure we're on a page that still exists
            if (mCurrentPage >= boundsSize)
            {
                SetCurrentItem(boundsSize - 1);
                return;
            }

            var countMinusOne = count - 1;
            var halfWidth = Width / 2f;
            var left = Left;
            var leftClip = left + mClipPadding;
            var width = Width;
            var height = Height;
            var right = left + width;
            var rightClip = right - mClipPadding;

            var page = mCurrentPage;
            float offsetPercent;
            if (mCurrentOffset <= halfWidth)
            {
                offsetPercent = 1.0f * mCurrentOffset / width;
            }
            else
            {
                page += 1;
                offsetPercent = 1.0f * (width - mCurrentOffset) / width;
            }

            var currentSelected = offsetPercent <= SELECTION_FADE_PERCENTAGE;
            var currentBold = offsetPercent <= BOLD_FADE_PERCENTAGE;
            var selectedPercent = (SELECTION_FADE_PERCENTAGE - offsetPercent) / SELECTION_FADE_PERCENTAGE;

            //Verify if the current view must be clipped to the screen
            var curPageBound = bounds[mCurrentPage];
            var curPageWidth = curPageBound.Right - curPageBound.Left;
            if (curPageBound.Left < leftClip) ClipViewOnTheLeft(curPageBound, curPageWidth, left);
            if (curPageBound.Right > rightClip) ClipViewOnTheRight(curPageBound, curPageWidth, right);

            //Left views starting from the current position
            if (mCurrentPage > 0)
                for (var i = mCurrentPage - 1; i >= 0; i--)
                {
                    var bound = bounds[i];
                    //Is left side is outside the screen
                    if (bound.Left < leftClip)
                    {
                        var w = bound.Right - bound.Left;
                        //Try to clip to the screen (left side)
                        ClipViewOnTheLeft(bound, w, left);
                        //Except if there's an intersection with the right view
                        var rightBound = bounds[i + 1];
                        //Intersection
                        if (bound.Right + mTitlePadding > rightBound.Left)
                        {
                            bound.Left = rightBound.Left - w - mTitlePadding;
                            bound.Right = bound.Left + w;
                        }
                    }
                }

            //Right views starting from the current position
            if (mCurrentPage < countMinusOne)
                for (var i = mCurrentPage + 1; i < count; i++)
                {
                    var bound = bounds[i];
                    //If right side is outside the screen
                    if (bound.Right > rightClip)
                    {
                        var w = bound.Right - bound.Left;
                        //Try to clip to the screen (right side)
                        ClipViewOnTheRight(bound, w, right);
                        //Except if there's an intersection with the left view
                        var leftBound = bounds[i - 1];
                        //Intersection
                        if (bound.Left - mTitlePadding < leftBound.Right)
                        {
                            bound.Left = leftBound.Right + mTitlePadding;
                            bound.Right = bound.Left + w;
                        }
                    }
                }

            //Now draw views
            //int colorTextAlpha = mColorText >>> 24;
            var colorTextAlpha = mColorText >> 24;
            for (var i = 0; i < count; i++)
            {
                //Get the title
                var bound = bounds[i];
                //Only if one side is visible
                if (bound.Left > left && bound.Left < right || bound.Right > left && bound.Right < right)
                {
                    var currentPage = i == page;
                    //Only set bold if we are within bounds
                    mPaintText.FakeBoldText = currentPage && currentBold && mBoldText;

                    //Draw text as unselected
                    mPaintText.Color = mColorText;
                    if (currentPage && currentSelected)
                        mPaintText.Alpha = colorTextAlpha - (int) (colorTextAlpha * selectedPercent);
                    canvas.DrawText(mTitleProvider.GetTitle(i), bound.Left, bound.Bottom + mTopPadding, mPaintText);

                    //If we are within the selected bounds draw the selected text
                    if (currentPage && currentSelected)
                    {
                        mPaintText.Color = mColorSelected;
                        mPaintText.Alpha = (int) ((mColorSelected >> 24) * selectedPercent);
                        canvas.DrawText(mTitleProvider.GetTitle(i), bound.Left, bound.Bottom + mTopPadding, mPaintText);
                    }
                }
            }

            //Draw the footer line
            mPath = new Path();
            mPath.MoveTo(0, height - mFooterLineHeight / 2f);
            mPath.LineTo(width, height - mFooterLineHeight / 2f);
            mPath.Close();
            canvas.DrawPath(mPath, mPaintFooterLine);

            switch (mFooterIndicatorStyle)
            {
                case IndicatorStyle.Triangle:
                    mPath = new Path();
                    mPath.MoveTo(halfWidth, height - mFooterLineHeight - mFooterIndicatorHeight);
                    mPath.LineTo(halfWidth + mFooterIndicatorHeight, height - mFooterLineHeight);
                    mPath.LineTo(halfWidth - mFooterIndicatorHeight, height - mFooterLineHeight);
                    mPath.Close();
                    canvas.DrawPath(mPath, mPaintFooterIndicator);
                    break;

                case IndicatorStyle.Underline:
                    if (!currentSelected || page >= boundsSize) break;

                    var underlineBounds = bounds[page];
                    mPath = new Path();
                    mPath.MoveTo(underlineBounds.Left - mFooterIndicatorUnderlinePadding, height - mFooterLineHeight);
                    mPath.LineTo(underlineBounds.Right + mFooterIndicatorUnderlinePadding, height - mFooterLineHeight);
                    mPath.LineTo(underlineBounds.Right + mFooterIndicatorUnderlinePadding,
                        height - mFooterLineHeight - mFooterIndicatorHeight);
                    mPath.LineTo(underlineBounds.Left - mFooterIndicatorUnderlinePadding,
                        height - mFooterLineHeight - mFooterIndicatorHeight);
                    mPath.Close();

                    mPaintFooterIndicator.Alpha = (int) (0xFF * selectedPercent);
                    canvas.DrawPath(mPath, mPaintFooterIndicator);
                    mPaintFooterIndicator.Alpha = 0xFF;
                    break;
            }
        }

        public override bool OnTouchEvent(MotionEvent ev)
        {
            if (base.OnTouchEvent(ev)) return true;
            if (mViewPager == null || mViewPager.Adapter.Count == 0) return false;

            var action = ev.Action;

#pragma warning disable CS0618 // 'MotionEventCompat.ActionMask' est� obsoleto: 'deprecated'
            switch ((int) action & MotionEventCompat.ActionMask)
#pragma warning restore CS0618 // 'MotionEventCompat.ActionMask' est� obsoleto: 'deprecated'
            {
                case (int) MotionEventActions.Down:
#pragma warning disable CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    mActivePointerId = MotionEventCompat.GetPointerId(ev, 0);
#pragma warning restore CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    mLastMotionX = ev.GetX();
                    break;

                case (int) MotionEventActions.Move:
                {
#pragma warning disable CS0618 // 'MotionEventCompat.FindPointerIndex(MotionEvent, int)' est� obsoleto: 'deprecated'
                    var activePointerIndex = MotionEventCompat.FindPointerIndex(ev, mActivePointerId);
#pragma warning restore CS0618 // 'MotionEventCompat.FindPointerIndex(MotionEvent, int)' est� obsoleto: 'deprecated'
#pragma warning disable CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
                    var x = MotionEventCompat.GetX(ev, activePointerIndex);
#pragma warning restore CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
                    var deltaX = x - mLastMotionX;

                    if (!mIsDragging)
                        if (Math.Abs(deltaX) > mTouchSlop)
                            mIsDragging = true;

                    if (mIsDragging)
                    {
                        if (!mViewPager.IsFakeDragging) mViewPager.BeginFakeDrag();

                        mLastMotionX = x;

                        mViewPager.FakeDragBy(deltaX);
                    }

                    break;
                }

                case (int) MotionEventActions.Cancel:
                case (int) MotionEventActions.Up:
                    if (!mIsDragging)
                    {
                        var count = mViewPager.Adapter.Count;
                        var width = Width;
                        var halfWidth = width / 2f;
                        var sixthWidth = width / 6f;
                        var leftThird = halfWidth - sixthWidth;
                        var rightThird = halfWidth + sixthWidth;
                        var eventX = ev.GetX();

                        if (eventX < leftThird)
                        {
                            if (mCurrentPage > 0)
                            {
                                mViewPager.CurrentItem = mCurrentPage - 1;
                                return true;
                            }
                        }
                        else if (eventX > rightThird)
                        {
                            if (mCurrentPage < count - 1)
                            {
                                mViewPager.CurrentItem = mCurrentPage + 1;
                                return true;
                            }
                        }
                        else
                        {
                            //Middle third
                            if (mCenterItemClickListener != null)
                                mCenterItemClickListener.OnCenterItemClick(mCurrentPage);
                        }
                    }


                    mIsDragging = false;
                    mActivePointerId = INVALID_POINTER;
                    if (mViewPager.IsFakeDragging)
                        mViewPager.EndFakeDrag();
                    break;

#pragma warning disable CS0618 // 'MotionEventCompat.ActionPointerDown' est� obsoleto: 'deprecated'
                case MotionEventCompat.ActionPointerDown:
#pragma warning restore CS0618 // 'MotionEventCompat.ActionPointerDown' est� obsoleto: 'deprecated'
                {
#pragma warning disable CS0618 // 'MotionEventCompat.GetActionIndex(MotionEvent)' est� obsoleto: 'deprecated'
                    var index = MotionEventCompat.GetActionIndex(ev);
#pragma warning restore CS0618 // 'MotionEventCompat.GetActionIndex(MotionEvent)' est� obsoleto: 'deprecated'
#pragma warning disable CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
                    var x = MotionEventCompat.GetX(ev, index);
#pragma warning restore CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
                    mLastMotionX = x;
#pragma warning disable CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    mActivePointerId = MotionEventCompat.GetPointerId(ev, index);
#pragma warning restore CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    break;
                }

#pragma warning disable CS0618 // 'MotionEventCompat.ActionPointerUp' est� obsoleto: 'deprecated'
                case MotionEventCompat.ActionPointerUp:
#pragma warning restore CS0618 // 'MotionEventCompat.ActionPointerUp' est� obsoleto: 'deprecated'
#pragma warning disable CS0618 // 'MotionEventCompat.GetActionIndex(MotionEvent)' est� obsoleto: 'deprecated'
                    var pointerIndex = MotionEventCompat.GetActionIndex(ev);
#pragma warning restore CS0618 // 'MotionEventCompat.GetActionIndex(MotionEvent)' est� obsoleto: 'deprecated'
#pragma warning disable CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    var pointerId = MotionEventCompat.GetPointerId(ev, pointerIndex);
#pragma warning restore CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    if (pointerId == mActivePointerId)
                    {
                        var newPointerIndex = pointerIndex == 0 ? 1 : 0;
#pragma warning disable CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                        mActivePointerId = MotionEventCompat.GetPointerId(ev, newPointerIndex);
#pragma warning restore CS0618 // 'MotionEventCompat.GetPointerId(MotionEvent, int)' est� obsoleto: 'deprecated'
                    }
#pragma warning disable CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
#pragma warning disable CS0618 // 'MotionEventCompat.FindPointerIndex(MotionEvent, int)' est� obsoleto: 'deprecated'
                    mLastMotionX = MotionEventCompat.GetX(ev, MotionEventCompat.FindPointerIndex(ev, mActivePointerId));
#pragma warning restore CS0618 // 'MotionEventCompat.FindPointerIndex(MotionEvent, int)' est� obsoleto: 'deprecated'
#pragma warning restore CS0618 // 'MotionEventCompat.GetX(MotionEvent, int)' est� obsoleto: 'deprecated'
                    break;
            }

            return true;
        }

        /**
	     * Set bounds for the right textView including clip padding.
	     *
	     * @param curViewBound
	     *            current bounds.
	     * @param curViewWidth
	     *            width of the view.
	     */
        private void ClipViewOnTheRight(RectF curViewBound, float curViewWidth, int right)
        {
            curViewBound.Right = right - mClipPadding;
            curViewBound.Left = curViewBound.Right - curViewWidth;
        }

        /**
	     * Set bounds for the left textView including clip padding.
	     *
	     * @param curViewBound
	     *            current bounds.
	     * @param curViewWidth
	     *            width of the view.
	     */
        private void ClipViewOnTheLeft(RectF curViewBound, float curViewWidth, int left)
        {
            curViewBound.Left = left + mClipPadding;
            curViewBound.Right = mClipPadding + curViewWidth;
        }

        /**
	     * Calculate views bounds and scroll them according to the current index
	     *
	     * @param paint
	     * @param currentIndex
	     * @return
	     */
        private List<RectF> CalculateAllBounds(Paint paint)
        {
            var list = new List<RectF>();
            //For each views (If no values then add a fake one)
            var count = mViewPager.Adapter.Count;
            var width = Width;
            var halfWidth = width / 2;

            for (var i = 0; i < count; i++)
            {
                var bounds = CalcBounds(i, paint);
                var w = bounds.Right - bounds.Left;
                var h = bounds.Bottom - bounds.Top;
                bounds.Left = halfWidth - w / 2 - mCurrentOffset + (i - mCurrentPage) * width;
                bounds.Right = bounds.Left + w;
                bounds.Top = 0;
                bounds.Bottom = h;
                list.Add(bounds);
            }

            return list;
        }

        /**
	     * Calculate the bounds for a view's title
	     *
	     * @param index
	     * @param paint
	     * @return
	     */
        private RectF CalcBounds(int index, Paint paint)
        {
            //Calculate the text bounds
            var bounds = new RectF
            {
                Right = paint.MeasureText(mTitleProvider.GetTitle(index)),
                Bottom = paint.Descent() - paint.Ascent()
            };
            return bounds;
        }

        /**
	     * Set a callback listener for the center item click.
	     *
	     * @param listener Callback instance.
	     */
        public void SetOnCenterItemClickListener(IOnCenterItemClickListener listener)
        {
            mCenterItemClickListener = listener;
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            //Measure our width in whatever mode specified
            var measuredWidth = MeasureSpec.GetSize(widthMeasureSpec);

            //Determine our height
            float height = 0;
            var heightSpecMode = MeasureSpec.GetMode(heightMeasureSpec);
            if (heightSpecMode == MeasureSpecMode.Exactly)
            {
                //We were told how big to be
                height = MeasureSpec.GetSize(heightMeasureSpec);
            }
            else
            {
                //Calculate the text bounds
                var bounds = new RectF
                {
                    Bottom = mPaintText.Descent() - mPaintText.Ascent()
                };
                height = bounds.Bottom - bounds.Top + mFooterLineHeight + mFooterPadding + mTopPadding;
                if (mFooterIndicatorStyle != IndicatorStyle.None) height += mFooterIndicatorHeight;
            }

            var measuredHeight = (int) height;

            SetMeasuredDimension(measuredWidth, measuredHeight);
        }

        protected override void OnRestoreInstanceState(IParcelable state)
        {
            try
            {
                var savedState = (SavedState) state;
                base.OnRestoreInstanceState(savedState.SuperState);
                mCurrentPage = savedState.CurrentPage;
            }
            catch
            {
                base.OnRestoreInstanceState(state);
                // Ignore, this needs to support IParcelable...
            }

            RequestLayout();
        }

        protected override IParcelable OnSaveInstanceState()
        {
            var superState = base.OnSaveInstanceState();
            var savedState = new SavedState(superState)
            {
                CurrentPage = mCurrentPage
            };
            return savedState;
        }

        /**
	     * Interface for a callback when the center item has been clicked.
	     */
        public interface IOnCenterItemClickListener
        {
            /**
	         * Callback when the center item has been clicked.
	         *
	         * @param position Position of the current center item.
	         */
            void OnCenterItemClick(int position);
        }

        public class SavedState : BaseSavedState
        {
            public SavedState(IParcelable superState) : base(superState)
            {
            }

            public SavedState(Parcel parcel) : base(parcel)
            {
                CurrentPage = parcel.ReadInt();
            }

            public int CurrentPage { get; set; }

            public override void WriteToParcel(Parcel dest, ParcelableWriteFlags flags)
            {
                base.WriteToParcel(dest, flags);
                dest.WriteInt(CurrentPage);
            }

            [ExportField("CREATOR")]
            private static SavedStateCreator InitializeCreator()
            {
                return new SavedStateCreator();
            }

            private class SavedStateCreator : Object, IParcelableCreator
            {
                public Object CreateFromParcel(Parcel source)
                {
                    return new SavedState(source);
                }

                public Object[] NewArray(int size)
                {
                    return new SavedState[size];
                }
            }
        }
    }
}