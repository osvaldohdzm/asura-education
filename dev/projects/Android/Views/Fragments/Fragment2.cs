﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class Fragment2 : Fragment
    {
        private Button btncreate;
        private Button btnsign;
        private EditText txtPassword;
        private EditText txtusername;

        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.fragment2, container, false);

            btnsign = view.FindViewById<Button>(Resource.Id.btnloglogin);
            btncreate = view.FindViewById<Button>(Resource.Id.btnlogreg);
            txtusername = view.FindViewById<EditText>(Resource.Id.txtlogusername);
            txtPassword = view.FindViewById<EditText>(Resource.Id.txtlogpassword);

            btncreate.Click += Btncreate_Click;
            btnsign.Click += Btnsign_Click;

            return view;
        }


        private async void Btnsign_Click(object sender, EventArgs e)
        {
            var client = new HttpClient();
            var uri = new Uri(string.Format("https://webapi20180814072429.azurewebsites.net/api/Login?Username=" +
                                            txtusername.Text + "&Password=" + txtPassword.Text));
            HttpResponseMessage response;
            ;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            response = await client.GetAsync(uri);
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim('"');
                Toast.MakeText(view.Context, errorMessage1, ToastLength.Long).Show();
            }
            else
            {
                var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim('"');
                Toast.MakeText(view.Context, errorMessage1, ToastLength.Long).Show();
            }
        }

        private void Btncreate_Click(object sender, EventArgs e)
        {
            var myActivity = (HomeActivity) Activity;
            myActivity.ShowFragment4();
        }
    }
}