﻿using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Data;
using Xamarin.Facebook.Login.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class LoginFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var database1 = new Database();

            var view = inflater.Inflate(Resource.Layout.login, container, false);
            var button1 = view.FindViewById<Button>(Resource.Id.email_sign_in_button);
            var button2 = view.FindViewById<Button>(Resource.Id.btnLinkToRegisterScreen);
            var button = view.FindViewById<LoginButton>(Resource.Id.login_button_facebook);
            var myActivity = (LoginRegisterActivity) Activity;
            var snackBar = Snackbar.Make(myActivity.FindViewById(Android.Resource.Id.Content), "No hay conexión",
                Snackbar.LengthIndefinite);
            snackBar.Dismiss();
            if (myActivity.IsConnected())
            {
                button1.Click += delegate
                {
                    database1.CreateUser();
                    StartActivity(new Intent(Activity, typeof(HomeActivity)));
                };
                button2.Click += delegate { myActivity.SecondFragment(); };
            }
            else
            {
                snackBar.Show();
                snackBar.SetAction("OK", v => { });
                RefreshFragment();
            }

            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
        }

        private async void RefreshFragment()
        {
            var fragmentLogin = new LoginFragment();
            var myActivity = (LoginRegisterActivity) Activity;

            while (!myActivity.IsConnected()) await Task.Delay(1000);
            myActivity.ChangeFragment(fragmentLogin);
        }
    }
}