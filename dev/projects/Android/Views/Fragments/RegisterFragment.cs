﻿using Android.OS;
using Android.Support.V4.App;
using Android.Text;
using Android.Text.Method;
using Android.Views;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class RegisterFragment : Fragment
    {
        private TextView _dateDisplay;
        private Button _dateSelectButton;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.register, container, false);

            var Text1 = "Aceptar <a href=\"http://google.com\">terminos y condiciones.</a>";

            var spinner = view.FindViewById<Spinner>(Resource.Id.spinner);
            //string theprompt = view.Context.GetString(Resource.String.car_prompt);
            //spinner.Prompt = theprompt;

            string[] generos = {"Masculino", "Femenino"};
            spinner.ItemSelected += Spinner_ItemSelected;
            var adapter = ArrayAdapter.CreateFromResource(view.Context, Resource.Array.genre_array,
                Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            /*
            TextView TextView1 = view.FindViewById<TextView>(Resource.Id.text1);
            TextView1.TextFormatted = Html.FromHtml(Text1);
            TextView1.MovementMethod = LinkMovementMethod.Instance;
            */

            var TextView1 = view.FindViewById<CheckBox>(Resource.Id.checkbox);
#pragma warning disable CS0618 // 'Html.FromHtml(string)' está obsoleto: 'deprecated'
            TextView1.TextFormatted = Html.FromHtml(Text1);
#pragma warning restore CS0618 // 'Html.FromHtml(string)' está obsoleto: 'deprecated'
            TextView1.MovementMethod = LinkMovementMethod.Instance;


            _dateDisplay = view.FindViewById<TextView>(Resource.Id.date_display);
            _dateSelectButton = view.FindViewById<Button>(Resource.Id.date_select_button);

            _dateSelectButton.Click += delegate
            {
                var myActivity = (LoginRegisterActivity) Activity;
                myActivity.DateFrag(_dateDisplay);
            };


            var button3 = view.FindViewById<Button>(Resource.Id.register_email_login_in_button);
            button3.Click += delegate
            {
                var myActivity = (LoginRegisterActivity) Activity;
                myActivity.ChangeFragment(myActivity.fragmentLogin);
                Toast.MakeText(view.Context, "Registro exitoso.", ToastLength.Long).Show();
            };


            return view;
        }

        private void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = sender as Spinner;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
        }
    }
}