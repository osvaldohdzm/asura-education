﻿using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class AnimatedIntroFragment : Fragment
    {
        private AnimationDrawable animation;

        private ImageView imageView;

        //Animations
        private Animation startAnimation;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.page_intro, container, false);


            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            //Definicion de animacion   
            imageView = View.FindViewById<ImageView>(Resource.Id.introimage1);
            startAnimation = AnimationUtils.LoadAnimation(View.Context, Resource.Animation.intro_animation);
            animation = (AnimationDrawable) imageView.Drawable;
        }

        public override void OnResume()
        {
            base.OnResume();
            imageView.StartAnimation(startAnimation);
            animation.Start();
        }
    }
}