﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Models;
using Firebase.Xamarin.Database;
using Firebase.Xamarin.Database.Query;

namespace AsuraEducationPlatform.Droid.Views
{
    public class Fragment3 : Fragment
    {
        private const string FirebaseURL = "https://asura-education-app.firebaseio.com/";
        private ProgressBar circular_progress;
        private readonly List<Topics> list_topics = new List<Topics>();

        private List<User> list_users = new List<User>();
        private string singleText = "";
        private TextView textView2;
        private TextView textView3;
        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.fragment3, container, false);

            circular_progress = view.FindViewById<ProgressBar>(Resource.Id.circularProgress);

            var linear = view.FindViewById<RelativeLayout>(Resource.Id.linear1);

            textView2 = new TextView(view.Context) {Id = 9002};
            var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.WrapContent);
            linear.AddView(textView2, param);
            textView2.SetTextColor(new Color(ContextCompat.GetColor(view.Context, Resource.Color.dark_gray)));

            textView3 = new TextView(view.Context) {Id = 9003};
            linear.AddView(textView3, param);
            textView2.SetTextColor(new Color(ContextCompat.GetColor(view.Context, Resource.Color.dark_gray)));

            Datos();


            return view;
        }


        private async void Datos()
        {
            await LoadData();
        }

        private async Task LoadData()
        {
            circular_progress.Visibility = ViewStates.Visible;
            textView2.Visibility = ViewStates.Invisible;

            var firebase = new FirebaseClient(FirebaseURL);
            var items = await firebase
                .Child("Topics")
                .OnceAsync<Topics>();


            list_topics.Clear();
            foreach (var item in items)
            {
                var topics = new Topics();
                topics.CourseTopicsListID = item.Object.CourseTopicsListID;


                list_topics.Add(topics);
            }

            foreach (var s in list_topics) singleText = singleText + s.CourseTopicsListID;

            textView2.Text = singleText;

            circular_progress.Visibility = ViewStates.Invisible;
            textView2.Visibility = ViewStates.Visible;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);
            inflater.Inflate(Resource.Menu.menu_main, menu);
        }


        private async void DeleteUser(string uid)
        {
            var firebase = new FirebaseClient(FirebaseURL);
            await firebase.Child("users").Child(uid).DeleteAsync();
            await LoadData();
        }

        private async void UpdateUser(string uid, string name, string email)
        {
            var firebase = new FirebaseClient(FirebaseURL);
            await firebase.Child("users").Child(uid).Child("name").PutAsync(name);
            await firebase.Child("users").Child(uid).Child("email").PutAsync(email);

            await LoadData();
        }
    }
}