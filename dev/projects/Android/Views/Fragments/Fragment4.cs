﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Models;
using Newtonsoft.Json;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class Fragment4 : Fragment
    {
        private Button btncreate;
        private EditText txtPassword;
        private EditText txtusername;
        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.fragment4, container, false);
            txtusername = view.FindViewById<EditText>(Resource.Id.txtsaveusername);
            txtPassword = view.FindViewById<EditText>(Resource.Id.txtsavepassword);
            btncreate = view.FindViewById<Button>(Resource.Id.btnsavecreate);
            btncreate.Click += Btncreate_Click;

            return view;
        }

        private async void Btncreate_Click(object sender, EventArgs e)
        {
            var url = "http://webapi20180814072429.azurewebsites.net/api/Login";
            var log = new Login();
            log.Username = txtusername.Text;
            log.Password = txtPassword.Text;
            var json = JsonConvert.SerializeObject(log);

            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Post;
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            var client = new HttpClient();
            HttpResponseMessage response = await client.SendAsync(request);
        
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                var Message = "Registro exitoso en app";
                Toast.MakeText(view.Context, Message, ToastLength.Long).Show();
            }
            else
            {
                var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim('"');
                Toast.MakeText(view.Context, errorMessage1, ToastLength.Long).Show();
            }

            var parameters = new Dictionary<string, string>
                {{"Username", txtusername.Text}, {"Password", txtPassword.Text}};

        }
    }
}