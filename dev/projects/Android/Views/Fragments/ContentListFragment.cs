﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Data;
using Fragment = Android.Support.V4.App.Fragment;

namespace AsuraEducationPlatform.Droid.Views
{
    public class ContentListFragment : Fragment
    {
        private ContentListAdapter adapter;
        private List<string> content;
        private ListView courseListView;
        private List<string> ejercicios;
        private List<string> examenes;
        private Context myContext;
        private List<string> recursos;

        private readonly int selectedID;
        private List<string> temas;
        private View view;

        public ContentListFragment(int optionID)
        {
            selectedID = optionID;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.listscreen, container, false);

            myContext = view.Context;
            var activity = (Activity) view.Context;

            courseListView = view.FindViewById<ListView>(Resource.Id.myListView);

            content = Getcontent();
            adapter = new ContentListAdapter(Activity, content);

            courseListView.Adapter = adapter;

            courseListView.ItemClick += CourseListView_ItemClick;


            return view;
        }

        private void CourseListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var selected = content[e.Position];
            Toast.MakeText(myContext, selected, ToastLength.Short).Show();
        }

        private List<string> Getcontent()
        {
            List<string> mContent;
            var database1 = new Database();
            if (selectedID == 1)
            {
                mContent = database1.courseTopics;
                return mContent;
            }

            if (selectedID == 3)
            {
                mContent = database1.courseTopics;
                return mContent;
            }

            if (selectedID == 4)
            {
                mContent = database1.courseTopics;
                return mContent;
            }

            return null;
        }
    }
}