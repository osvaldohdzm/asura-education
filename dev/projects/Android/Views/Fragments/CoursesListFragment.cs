﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Data;
using AsuraEducationPlatform.Models;
using Fragment = Android.Support.V4.App.Fragment;

namespace AsuraEducationPlatform.Droid.Views
{
    public class CoursesListFragment : Fragment
    {
        private CoursesListAdapter adapter;
        private ListView courseListView;
        private List<Course> courses;
        private Context myContext;
        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public static CoursesListFragment NewInstance()
        {
            var frag3 = new CoursesListFragment {Arguments = new Bundle()};
            return frag3;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.listscreen, container, false);

            myContext = view.Context;
            var activity = (Activity) view.Context;

            courseListView = view.FindViewById<ListView>(Resource.Id.myListView);

            courses = GetCourses();
            adapter = new CoursesListAdapter(Activity, courses);

            courseListView.Adapter = adapter;

            courseListView.ItemClick += CourseListView_ItemClick;


            return view;
        }

        private void CourseListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var selected = courses[e.Position];
            Toast.MakeText(myContext, selected.ToString(), ToastLength.Short).Show();
        }

        private List<Course> GetCourses()
        {
            var database1 = new Database();
            var fismatCatalog = database1.categoryFismat;

            return fismatCatalog;
        }
    }
}