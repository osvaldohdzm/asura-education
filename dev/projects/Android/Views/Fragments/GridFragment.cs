﻿using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class GridFragment : Fragment
    {
        private LinearLayout linearLayout;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static GridFragment NewInstance()
        {
            var frag2 = new GridFragment {Arguments = new Bundle()};
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.grid_layout, container, false);

            linearLayout = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            linearLayout.Click += delegate { SectionCourseTitleClick(1); };
            linearLayout = view.FindViewById<LinearLayout>(Resource.Id.linearLayout2);
            linearLayout.Click += delegate { StartActivity(new Intent(view.Context, typeof(QuizzActivity))); };
            linearLayout = view.FindViewById<LinearLayout>(Resource.Id.linearLayout3);
            linearLayout.Click += delegate { SectionCourseTitleClick(3); };
            linearLayout = view.FindViewById<LinearLayout>(Resource.Id.linearLayout4);
            linearLayout.Click += delegate { SectionCourseTitleClick(4); };

            return view;
        }

        public void SectionCourseTitleClick(int positionNumber)
        {
            var myActivity = (HomeActivity) Activity;
            myActivity.ShowFragmentOption(positionNumber);
        }
    }
}