﻿using System;
using System.Collections.Generic;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Data;
using AsuraEducationPlatform.Droid.Libraries;
using AsuraEducationPlatform.Models;

namespace AsuraEducationPlatform.Droid.Views
{
    public class HomeFragment : Fragment
    {
        private static readonly Database database1 = new Database();
        private static readonly List<Course> multidisciplinaryCatalg = database1.categoryMultidisciplinary;
        private static readonly List<Course> socialesCatalog = database1.categorySocial;
        private static readonly List<Course> fismatCatalog = database1.categoryFismat;
        private static readonly List<Course> biomedCatalog = database1.categoryBiomed;
        private static readonly List<Course> humanartCatalog = database1.categoryHumanity;

        // ViewPager
        private readonly Random RANDOM = new Random();
        public ImageAdapter adapter;

        // Adapter that accesses the data set
        private CourseCatalogAdapter mAdapter;
        private CourseCatalogAdapter mAdapter2;
        private CourseCatalogAdapter mAdapter3;
        private CourseCatalogAdapter mAdapter4;
        private CourseCatalogAdapter mAdapter5;


        // Content data that is managed by the adapter
        private CourseCatalog mCourseCatalog;
        private CourseCatalog mCourseCatalog2;
        private CourseCatalog mCourseCatalog3;
        private CourseCatalog mCourseCatalog4;
        private CourseCatalog mCourseCatalog5;
        public PageIndicator mIndicator;

        // Layout manager that lays out each card in the RecyclerView:
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.LayoutManager mLayoutManager2;
        private RecyclerView.LayoutManager mLayoutManager3;
        private RecyclerView.LayoutManager mLayoutManager4;
        private RecyclerView.LayoutManager mLayoutManager5;
        public ViewPager mPager;

        private RecyclerView mRecyclerView;
        private RecyclerView mRecyclerView2;
        private RecyclerView mRecyclerView3;
        private RecyclerView mRecyclerView4;
        private RecyclerView mRecyclerView5;

        public static HomeFragment NewInstance()
        {
            var frag1 = new HomeFragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.home_fragment, container, false);

            var text1 = view.FindViewById<TextView>(Resource.Id.textView1);
            text1.Click += delegate { SectionCourseTitleClick(1); };
            var text2 = view.FindViewById<TextView>(Resource.Id.textView2);
            text2.Click += delegate { SectionCourseTitleClick(2); };
            var text3 = view.FindViewById<TextView>(Resource.Id.textView3);
            text3.Click += delegate { SectionCourseTitleClick(3); };
            var text4 = view.FindViewById<TextView>(Resource.Id.textView4);
            text4.Click += delegate { SectionCourseTitleClick(4); };

            // Instantiate the photo catalog:
            mCourseCatalog = new CourseCatalog(multidisciplinaryCatalg);
            mCourseCatalog2 = new CourseCatalog(socialesCatalog);
            mCourseCatalog3 = new CourseCatalog(fismatCatalog);
            mCourseCatalog4 = new CourseCatalog(biomedCatalog);
            mCourseCatalog5 = new CourseCatalog(humanartCatalog);


            // Get our RecyclerView layout:
            mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mRecyclerView2 = view.FindViewById<RecyclerView>(Resource.Id.recyclerView2);
            mRecyclerView3 = view.FindViewById<RecyclerView>(Resource.Id.recyclerView3);
            mRecyclerView4 = view.FindViewById<RecyclerView>(Resource.Id.recyclerView4);
            mRecyclerView5 = view.FindViewById<RecyclerView>(Resource.Id.recyclerView5);


            // -------------------------------------------------
            // Layout Manager Setup
            // Use the built-in linear layout manager:
            // Original code
            // 
            // mLayoutManager = new LinearLayoutManager(view.Context);
            //
            // Modidified code for orientation of RecyclerView
            mLayoutManager = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);
            mLayoutManager2 = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);
            mLayoutManager3 = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);
            mLayoutManager4 = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);
            mLayoutManager5 = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);

            // Or use the built-in grid layout manager (two horizontal rows):
            // mLayoutManager = new GridLayoutManager
            //        (this, 2, GridLayoutManager.Horizontal, false);

            // Plug the layout manager into the RecyclerView:           
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mRecyclerView2.SetLayoutManager(mLayoutManager2);
            mRecyclerView3.SetLayoutManager(mLayoutManager3);
            mRecyclerView4.SetLayoutManager(mLayoutManager4);
            mRecyclerView5.SetLayoutManager(mLayoutManager5);

            // -------------------------------------------------
            // Adapter Setup:

            // Create an adapter for the RecyclerView, and pass it the
            // data set (the photo catalog) to manage:
            mAdapter = new CourseCatalogAdapter(mCourseCatalog, 1);
            mAdapter2 = new CourseCatalogAdapter(mCourseCatalog2, 2);
            mAdapter3 = new CourseCatalogAdapter(mCourseCatalog3, 3);
            mAdapter4 = new CourseCatalogAdapter(mCourseCatalog4, 4);
            mAdapter5 = new CourseCatalogAdapter(mCourseCatalog5, 5);

            // Register the item click handler (below) with the adapter:
            mAdapter.ItemClick += OnItemClick;
            mAdapter2.ItemClick += OnItemClick;
            mAdapter3.ItemClick += OnItemClick;
            mAdapter4.ItemClick += OnItemClick;
            mAdapter5.ItemClick += OnItemClick;


            // Plug the adapter into the RecyclerView:
            mRecyclerView.SetAdapter(mAdapter);
            mRecyclerView2.SetAdapter(mAdapter2);
            mRecyclerView3.SetAdapter(mAdapter3);
            mRecyclerView4.SetAdapter(mAdapter4);
            mRecyclerView5.SetAdapter(mAdapter5);

            // Viewpager -----------------
            // mAdapter = new TestFragmentAdapter (SupportFragmentManager);
            adapter = new ImageAdapter(view.Context);

            mPager = view.FindViewById<ViewPager>(Resource.Id.pager);
            // mPager.Adapter = mAdapter;
            mPager.Adapter = adapter;
            var indicator = view.FindViewById<CirclePageIndicator>(Resource.Id.indicator);
            mIndicator = indicator;
            indicator.SetViewPager(mPager);
            indicator.SetSnap(true);


            return view;
        }

        // Handler for the item click event:
        private void OnItemClick(object sender, int position)
        {
            var theAdapter = (CourseCatalogAdapter) sender;


            // Display a toast that briefly shows the enumeration of the selected photo:
            var photoNum = position + 1;
            Toast.MakeText(View.Context, "This is photo number" + photoNum + " es " + theAdapter.numberRecyclerview,
                ToastLength.Short).Show();

            var myActivity = (HomeActivity) Activity;
            myActivity.ShowListGridFragment();
        }

        public void SectionCourseTitleClick(int positionNumber)
        {
            var myActivity = (HomeActivity) Activity;
            myActivity.ShowListFragment();
        }
    }
}