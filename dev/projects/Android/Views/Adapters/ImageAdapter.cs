using Android.Content;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace AsuraEducationPlatform.Droid.Views
{
    public class ImageAdapter : PagerAdapter
    {
        private readonly Context context;

        private readonly int[] imageList =
        {
            Resource.Drawable.cover_course01,
            Resource.Drawable.cover_course03,
            Resource.Drawable.cover_course04
        };

        public ImageAdapter(Context context)
        {
            this.context = context;
        }

        public override int Count => imageList.Length;

        public override bool IsViewFromObject(View view, Object objectValue)
        {
            return view == (ImageView) objectValue;
        }

#pragma warning disable CS0672 // El miembro 'ImageAdapter.InstantiateItem(View, int)' invalida el miembro obsoleto 'PagerAdapter.InstantiateItem(View, int)'. Agregue el atributo Obsolete a 'ImageAdapter.InstantiateItem(View, int)'.
        public override Object InstantiateItem(View container, int position)
#pragma warning restore CS0672 // El miembro 'ImageAdapter.InstantiateItem(View, int)' invalida el miembro obsoleto 'PagerAdapter.InstantiateItem(View, int)'. Agregue el atributo Obsolete a 'ImageAdapter.InstantiateItem(View, int)'.
        {
            var imageView = new ImageView(context);
            imageView.SetScaleType(ImageView.ScaleType.CenterCrop);
            imageView.SetImageResource(imageList[position]);
            ((ViewPager) container).AddView(imageView, 0);
            return imageView;
        }

#pragma warning disable CS0672 // El miembro 'ImageAdapter.DestroyItem(View, int, Object)' invalida el miembro obsoleto 'PagerAdapter.DestroyItem(View, int, Object)'. Agregue el atributo Obsolete a 'ImageAdapter.DestroyItem(View, int, Object)'.
        public override void DestroyItem(View container, int position, Object objectValue)
#pragma warning restore CS0672 // El miembro 'ImageAdapter.DestroyItem(View, int, Object)' invalida el miembro obsoleto 'PagerAdapter.DestroyItem(View, int, Object)'. Agregue el atributo Obsolete a 'ImageAdapter.DestroyItem(View, int, Object)'.
        {
            ((ViewPager) container).RemoveView((ImageView) objectValue);
        }
    }
}