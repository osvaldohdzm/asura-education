﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace AsuraEducationPlatform.Droid.Views
{
    public class ContentListAdapter : BaseAdapter
    {
        private readonly List<string> content;
        protected Activity ctx;

        public ContentListAdapter(Activity _ctx, List<string> _content)
        {
            ctx = _ctx;
            content = _content;
        }

        public override int Count => content.Count;

        public override Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.content_row, parent, false);

            view.FindViewById<TextView>(Resource.Id.numberText).Text = (position + 1).ToString();
            view.FindViewById<TextView>(Resource.Id.textHeader).Text = content[position];

            return view;
        }
    }
}