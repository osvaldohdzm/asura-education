﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Models;
using Java.Lang;

namespace AsuraEducationPlatform.Droid.Views
{
    public class CoursesListAdapter : BaseAdapter
    {
        private readonly List<Course> courses;
        protected Activity ctx;

        public CoursesListAdapter(Activity _ctx, List<Course> _courses)
        {
            ctx = _ctx;
            courses = _courses;
        }

        public override int Count => courses.Count;

        public override Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;


            if (view == null)
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.courses_list_row, parent, false);
            var course = courses[position];

            view.FindViewById<ImageView>(Resource.Id.imgView).SetImageResource(course.CoursePicture);
            view.FindViewById<TextView>(Resource.Id.textHeader).Text = course.CourseTitle;
            view.FindViewById<TextView>(Resource.Id.textSub).Text = course.CourseLevel;

            return view;
        }
    }
}