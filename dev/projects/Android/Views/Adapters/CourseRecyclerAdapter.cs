using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Models;

namespace AsuraEducationPlatform.Droid.Views
{
    public class CourseViewHolder : RecyclerView.ViewHolder
    {
        public CourseViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            CoursePicture = itemView.FindViewById<ImageView>(Resource.Id.imageView);
            CourseTitle = itemView.FindViewById<TextView>(Resource.Id.textView);
            CourseLevel = itemView.FindViewById<TextView>(Resource.Id.textView2);
            itemView.Click += (sender, e) => listener(LayoutPosition);
        }

        public ImageView CoursePicture { get; }

        public TextView CourseTitle { get; }

        public TextView CourseLevel { get; }
    }

    public class AdapterEventArgs : EventArgs
    {
        public int Item { get; set; }

        public int NumberR { get; set; }
    }

    public class CourseCatalogAdapter : RecyclerView.Adapter
    {
        public CourseCatalog mCourseCatalog;
        public ViewGroup mparent;
        public int numberRecyclerview;

        public CourseCatalogAdapter(CourseCatalog CourseCatalog, int nRecyclerview)
        {
            mCourseCatalog = CourseCatalog;
            numberRecyclerview = nRecyclerview;
        }

        public override int ItemCount => mCourseCatalog.NumCourses;

        public event EventHandler<int> ItemClick;

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            mparent = parent;
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.photo_card_view, parent, false);
            var vh = new CourseViewHolder(itemView, OnClick);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as CourseViewHolder;
            var gradientType = GradientType.SweepGradient;
            var grad = new GradientDrawable();
            grad.SetOrientation(GradientDrawable.Orientation.TopBottom);
            grad.SetGradientType(gradientType);
            vh.CoursePicture.SetImageResource(mCourseCatalog[position].CoursePicture);
            vh.CourseTitle.Text = mCourseCatalog[position].CourseTitle;
            vh.CourseLevel.Text = mCourseCatalog[position].CourseLevel;
            if (position == 1 && numberRecyclerview == 1 || position == 0 && numberRecyclerview == 5)
            {
                vh.CoursePicture.SetColorFilter(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.transparent_gray)),
                    PorterDuff.Mode.SrcAtop);
                vh.CoursePicture.SetBackgroundColor(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.dark_gray)));
            }
            else if (numberRecyclerview == 1)
            {
                grad.SetColors(new int[]
                {
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_red)),
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_blue)),
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_green)),
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_orange)),
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_red))
                });
                vh.CoursePicture.Background = grad;
            }
            else if (numberRecyclerview == 2)
            {
                vh.CoursePicture.SetBackgroundColor(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_orange)));
            }
            else if (numberRecyclerview == 3)
            {
                vh.CoursePicture.SetBackgroundColor(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_blue)));
            }
            else if (numberRecyclerview == 4)
            {
                vh.CoursePicture.SetBackgroundColor(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_green)));
            }
            else if (numberRecyclerview == 5)
            {
                vh.CoursePicture.SetBackgroundColor(
                    new Color(ContextCompat.GetColor(mparent.Context, Resource.Color.local_red)));
            }
        }

        private void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }
    }
}