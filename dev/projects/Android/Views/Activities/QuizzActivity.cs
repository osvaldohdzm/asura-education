﻿using System.Collections.Generic;
using System.Net;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using AsuraEducationPlatform.Data;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "QuizzActivity", Theme = "@style/AppTheme")]
    public class QuizzActivity : Activity
    {
        private int Progress = 1;
        private int Quest;

        private int Score;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Init();
        }

        public void Init()
        {
            SetContentView(Resource.Layout.QuizzLayout);

            var preguntas = new List<string>();
            preguntas = getQuestions();

            var t1 = FindViewById<TextView>(Resource.Id.textView1);
            t1.Text = preguntas[Quest];

            var b = new List<Button>();
            var edit1 = FindViewById<TextView>(Resource.Id.editText1);
            edit1.Text = Progress + "/15";

            var img = FindViewById<ImageView>(Resource.Id.imageView1);
            var resourceId = (int) typeof(Resource.Drawable).GetField("breakingbad").GetValue(null);
            img.SetImageResource(resourceId);

            b.Add(FindViewById<Button>(Resource.Id.button1));
            b.Add(FindViewById<Button>(Resource.Id.button2));
            b.Add(FindViewById<Button>(Resource.Id.button3));
            b.Add(FindViewById<Button>(Resource.Id.button4));

            var i = 1;
            foreach (var bi in b)
            {
                bi.Text = preguntas[Quest + i];
                bi.Click += delegate { NextQuestion(bi); };
                ++i;
            }
        }

        public void NextQuestion(Button b)
        {
            var preguntasb = new List<string>();
            preguntasb = getQuestions();
            var intent = new Intent(this, typeof(GameOverActivity));
            var s = Score.ToString();

            if (b.Text == preguntasb[Quest + 2]) Score++;

            if (Progress >= 15)
            {
                if (Score <= 10)
                    intent.PutExtra("Won", "Sigue estudiando, Score : " + Score);
                else
                    intent.PutExtra("Won", "Bien hecho !, Score : " + Score);
                StartActivity(intent);
                Finish();
            }
            else
            {
                Quest = Quest + 6;
                Progress++;
                Init();
            }
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
            }

            return imageBitmap;
        }


        public List<string> getQuestions()
        {
            var database1 = new Database();
            var questions = database1.courseExcercises;
            return questions;
        }
    }
}