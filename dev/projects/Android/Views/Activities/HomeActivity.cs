﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using AsuraEducationPlatform.Views.Fragments;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme", LaunchMode = LaunchMode.SingleTop,
        HardwareAccelerated = true)]
    public class HomeActivity : AppCompatActivity
    {
        private DrawerLayout drawerLayout;
        private FragmentManager mFragmentManager;
        private NavigationView navigationView;

        private int oldPosition = -1;
        private IMenuItem previousItem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            mFragmentManager = SupportFragmentManager;


            SetContentView(Resource.Layout.home);
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                SetSupportActionBar(toolbar);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetHomeButtonEnabled(true);
            }

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            //Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            //setup navigation view
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //handle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                if (previousItem != null)
                    previousItem.SetChecked(false);

                navigationView.SetCheckedItem(e.MenuItem.ItemId);

                previousItem = e.MenuItem;

                if (e.MenuItem.ItemId == Resource.Id.nav_home_1)
                    ListItemClicked(1);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_2)
                    ListItemClicked(2);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_3)
                    ListItemClicked(3);

                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
                navigationView.SetCheckedItem(Resource.Id.nav_home_1);
                ListItemClicked(1);
            }
        }

        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
                return;

            oldPosition = position;

            Fragment fragment = null;
            switch (position)
            {
                case 1:
                    fragment = HomeFragment.NewInstance();
                    break;
                case 2:
                    fragment = new Fragment2 {Arguments = new Bundle()};
                    ;
                    break;
                case 3:
                    fragment = new Fragment3 {Arguments = new Bundle()};
                    ;
                    break;
                default:
                    fragment = HomeFragment.NewInstance();
                    break;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment)
                .Commit();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public void ShowListFragment()
        {
            var myFragment = new CoursesListFragment();
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowFragmentOption(int optionID)
        {
            var myFragment = new ContentListFragment(optionID);
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowListGridFragment()
        {
            var myFragment = new GridFragment();
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowFragment4()
        {
            Fragment fragment = new Fragment4 {Arguments = new Bundle()};
            ;
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment).AddToBackStack(null)
                .Commit();
        }
    }
}