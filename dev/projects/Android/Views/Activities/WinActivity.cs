﻿using Android.App;
using Android.OS;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "WinActivity")]
    public class WinActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.QuizzLayout);
        }
    }
}