﻿using Android.App;
using Android.OS;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "GameOverActivity", Theme = "@style/AppTheme")]
    public class GameOverActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.GameOverLayout);
            var text = FindViewById<TextView>(Resource.Id.textView1);
            var b1 = FindViewById<Button>(Resource.Id.button1);
            //            Button b2 = FindViewById<Button>(Resource.Id.button2);

            text.Text = Intent.GetStringExtra("Won");

            b1.Click += delegate
            {
                //var intent = new Intent(this, typeof(HomeActivity));
                //StartActivity(intent);
                Finish();
            };
        }
    }
}