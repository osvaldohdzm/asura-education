﻿using System;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainTheme.Login")]
    public class LoginRegisterActivity : AppCompatActivity, FragmentManager.IOnBackStackChangedListener
    {
        public LoginFragment fragmentLogin = new LoginFragment();
        public RegisterFragment fragmentRegister = new RegisterFragment();
        public Context mContext;
        private FragmentManager mFragmentManager;

        public void OnBackStackChanged()
        {
            throw new NotImplementedException();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.login_register);

            mFragmentManager = SupportFragmentManager;

            mContext = this;

            ChangeFragment(fragmentLogin);
        }

        public void SecondFragment()
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.FrameContainer, fragmentRegister)
                .AddToBackStack(null).Commit();
        }

        public void ChangeFragment(Fragment Fragmento)
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.FrameContainer, Fragmento)
                .AddToBackStack(null).Commit();
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();


            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.FrameContainer, fragmentLogin)
                .AddToBackStack(null).Commit();
        }

        public void DateFrag(TextView _dateDisplay)
        {
            var frag = DatePickerFragment.NewInstance(delegate(DateTime time)
            {
                _dateDisplay.Text = time.ToShortDateString();
            });
            frag.Show(FragmentManager, null);
        }

        public bool IsConnected()
        {
            var info = GetNetworkInfo(mContext);
            return info != null && info.IsConnected;
        }

        public static NetworkInfo GetNetworkInfo(Context context)
        {
            var cm = (ConnectivityManager) context.GetSystemService(ConnectivityService);
            return cm.ActiveNetworkInfo;
        }
    }
}