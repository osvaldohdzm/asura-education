﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using AppIntro;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainTheme.Intro")]
    public class SlideIntroActivity : AppIntro2
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var AnimatedLogoIntro = new AnimatedIntroFragment();

            AddSlide(AnimatedLogoIntro);
            // Bar color works
            // SetSeparatorColor(Color.ParseColor("#000000"));
            //SetBarColor(Color.ParseColor("#000000"));
            AddSlide(AppIntroFragment.NewInstance(GetString(Resource.String.title1),
                GetString(Resource.String.description1), Resource.Drawable.picture_intro1,
                Color.ParseColor("#3498DB")));
            AddSlide(AppIntroFragment.NewInstance(GetString(Resource.String.title2),
                GetString(Resource.String.description2), Resource.Drawable.picture_intro2,
                Color.ParseColor("#27AFB7")));
            AddSlide(AppIntroFragment.NewInstance(GetString(Resource.String.title3),
                GetString(Resource.String.description3), Resource.Drawable.picture_intro3,
                Color.ParseColor("#3FB34F")));
            AddSlide(AppIntroFragment.NewInstance(GetString(Resource.String.title4),
                GetString(Resource.String.description4), Resource.Drawable.picture_intro4,
                Color.ParseColor("#F55B5C")));
        }

        public void GetStarted(View v)
        {
            LoadMainActivity();
        }

        public override void OnDonePressed()
        {
            base.OnDonePressed();

            LoadMainActivity();
        }

        public override void OnSkipPressed()
        {
            base.OnSkipPressed();
            LoadMainActivity();
            //Toast.MakeText(ApplicationContext, GetString(Resource.String.skip), ToastLength.Short).Show();
        }

        public void LoadMainActivity()
        {
            StartActivity(new Intent(this, typeof(LoginRegisterActivity)));
            Finish();
        }
    }
}