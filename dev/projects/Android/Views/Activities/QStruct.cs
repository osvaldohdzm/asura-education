﻿

/*
 * Structure de Question
 *
 */

namespace AsuraEducationPlatform.Droid.Views
{
    internal class QStruct
    {
        public string answer;
        public string[] prop;
        public string question;
        public string url_img;

        public QStruct()
        {
        }

        public QStruct(string q, string[] p, string a, string u)
        {
            question = q;
            prop = p;
            answer = a;
            url_img = u;
        }
    }
}