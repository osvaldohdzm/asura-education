﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AsuraEducationPlatform.Models;
using Firebase.Xamarin.Database;
using Firebase.Xamarin.Database.Query;

namespace AsuraEducationPlatform.Data
{
    public class Database
    {
        private const string FirebaseURL = "https://asura-education-app.firebaseio.com/";
        public readonly List<Course> categoryBiomed;
        public readonly List<Course> categoryFismat;
        public readonly List<Course> categoryHumanity;

        public readonly List<Course> categoryMultidisciplinary;
        public readonly List<Course> categorySocial;
        public readonly List<string> courseExcercises;
        public readonly List<string> courseTopics;
        public readonly List<string> temporalTopics;

        private readonly List<Course> biomedCatalog = new List<Course>
        {
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course10_circle,
                CourseTitle = "Curso básico de biología",
                CourseLevel = "Nivel básico"
            }
        };

        private readonly List<string> excercises = new List<string>
        {
            "Funciones 1de la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones 2de la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de3 la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de4 la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 5la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 6la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 7la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 8la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 9la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 10la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 11la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 12la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 13la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 14la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro.",

            "Funciones de 15la ficha bibliografica:",
            "Resumir y memorizar el contenido de un libro.",
            "Escribir y resumir el contenido de un libro.",
            "Leer y memorizar el contenido de un libro.",
            "Localizar y conservar la información de un libro",
            "Escribir y resumir el contenido de un libro."
        };

        private readonly List<Course> fismatCatalog = new List<Course>
        {
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course03_circle,
                CourseTitle = "Introducción a la física",
                CourseLevel = "Nivel básico"
            },
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course04_circle,
                CourseTitle = "Introducción a la química",
                CourseLevel = "Nivel básico"
            },
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course05_circle,
                CourseTitle = "Introducción a las matemáticas",
                CourseLevel = "Nivel básico"
            },
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course06_circle,
                CourseTitle = "Fundamentos de habilidad matemática",
                CourseLevel = "Nivel básico"
            }
        };

        private readonly List<Course> humanartCatalog = new List<Course>
        {
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course07_circle,
                CourseTitle = "Introducción a la música",
                CourseLevel = "Nivel básico"
            }
        };

        private readonly List<Account> list_users = new List<Account>();

        private readonly List<Course> multidisciplinaryCatalog = new List<Course>
        {
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course01_circle,
                CourseTitle = "Curso definitivo para el examen de admisión a preparatoria o bachillerato",
                CourseLevel = "Nivel básico"
            },
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course02_circle,
                CourseTitle = "Fundamentos de inglés",
                CourseLevel = "Nivel básico"
            }
        };

        private readonly List<Course> socialesCatalog = new List<Course>
        {
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course08_circle,
                CourseTitle = "Introducción a la geografía",
                CourseLevel = "Nivel básico"
            },
            new Course
            {
                CoursePicture = Resource.Drawable.cover_course09_circle,
                CourseTitle = "Curso de formación física y ética",
                CourseLevel = "Nivel básico"
            }
        };

        private readonly List<string> topics = new List<string>
        {
            "¿Qué son las matemáticas?",
            "¿Para qué sirven?",
            "¿Cómo usarlas?"
        };

        public Database()
        {
            categoryMultidisciplinary = multidisciplinaryCatalog;
            categoryFismat = fismatCatalog;
            categoryBiomed = biomedCatalog;
            categorySocial = socialesCatalog;
            categoryHumanity = humanartCatalog;
            courseExcercises = excercises;
            courseTopics = topics;
            temporalTopics = new List<string>();
            temporalTopics.Add("Hola");
        }


        public async void CreateUser()
        {
            var user = new Account
            {
                AccountId = "0000000001",
                FirstName = "Osvaldo",
                Email = "osvaldo.hdz.m@outlook.com",
                BirthDate = "14/05/1996",
                SecondName = "Hernández",
                Password = "os23valdo1",
                SubscriptionLevel = "Subscribed",
                Type = "Administrador",
                PurchasedContentIDList = new[] {"00000001", "00000002", "00000003"}
            };
            var firebase = new FirebaseClient(FirebaseURL);
            await firebase.Child("Users/" + user.AccountId).PutAsync(user);
        }

        private async Task LoadData()
        {
            var firebase = new FirebaseClient(FirebaseURL);
            var items = await firebase.Child("Users").OnceAsync<Account>();
            list_users.Clear();
            foreach (var item in items)
            {
                var acc = new Account();
                acc.AccountId = item.Key;
                acc.FirstName = item.Object.FirstName;
                acc.Email = item.Object.Email;
                list_users.Add(acc);
            }
        }

        public async Task LoadDataAsync(int a)
        {
            var firebase = new FirebaseClient(FirebaseURL);

            var items = await firebase.Child("Topics").Child("CourseTopicsList").OrderByKey().OnceSingleAsync<string>();

            string tmpitem;
            temporalTopics.Clear();

            if (a == 1)
                foreach (var item in items)
                {
                    tmpitem = item.ToString();
                    //temporalTopics.Add(tmpitem);
                    temporalTopics.Add("Hola");
                }
        }

        private async void DeleteUser(string AccountId)
        {
            var firebase = new FirebaseClient(FirebaseURL);
            await firebase.Child("Users").Child(AccountId).DeleteAsync();
            await LoadData();
        }

        private async void UpdateUser(string AccountId, string FirstName, string Email)
        {
            var firebase = new FirebaseClient(FirebaseURL);
            await firebase.Child("Users").Child(AccountId).Child("FirstName").PutAsync(FirstName);
            await firebase.Child("Users").Child(AccountId).Child("Email").PutAsync(Email);
            await LoadData();
        }
    }
}