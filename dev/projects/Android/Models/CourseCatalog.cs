﻿using System;
using System.Collections.Generic;

namespace AsuraEducationPlatform.Models
{
    // Course catalog: holds image resource IDs and caption:
    public class CourseCatalog
    {
        private readonly List<Course> mCourses;

        // Random number generator for shuffling the Courses:
        private readonly Random mRandom;

        // Create an instance copy of the built-in Course list and
        // create the random number generator:
        public CourseCatalog(List<Course> catalog)
        {
            mCourses = catalog;
            mRandom = new Random();
        }

        // Return the number of Courses in the Course catalog:
        public int NumCourses => mCourses.Count;

        // Indexer (read only) for accessing a Course:
        public Course this[int i] => mCourses[i];

        // Pick a random Course and swap it with the top:
        public int RandomSwap()
        {
            // Save the Course at the top:
            var tmpCourse = mCourses[0];

            // Generate a next random index between 1 and 
            // Length (noninclusive):
            var rnd = mRandom.Next(1, mCourses.Count);

            // Exchange top Course with randomly-chosen Course:
            mCourses[0] = mCourses[rnd];
            mCourses[rnd] = tmpCourse;

            // Return the index of which Course was swapped with the top:
            return rnd;
        }

        // Shuffle the order of the Courses:
        public void Shuffle()
        {
            // Use the Fisher-Yates shuffle algorithm:
            for (var idx = 0; idx < mCourses.Count; ++idx)
            {
                // Save the Course at idx:
                var tmpCourse = mCourses[idx];

                // Generate a next random index between idx (inclusive) and 
                // Length (noninclusive):
                var rnd = mRandom.Next(idx, mCourses.Count);

                // Exchange Course at idx with randomly-chosen (later) Course:
                mCourses[idx] = mCourses[rnd];
                mCourses[rnd] = tmpCourse;
            }
        }
    }
}