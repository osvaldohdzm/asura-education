﻿namespace AsuraEducationPlatform.Models
{
    public class Account
    {
        public string AccountId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string SecondName { get; set; }
        public string BirthDate { get; set; }
        public string Password { get; set; }
        public string SubscriptionLevel { get; set; }
        public string Type { get; set; }
        public string[] PurchasedContentIDList { get; set; }
    }
}