namespace AsuraEducationPlatform.Models
{
    public class User
    {
        public string uid { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}