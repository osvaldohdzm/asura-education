﻿namespace AsuraEducationPlatform.Models
{
    internal class FacebookResult
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}