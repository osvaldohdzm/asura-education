﻿namespace AsuraEducationPlatform.Models
{
    public class Course
    {
        public int CourseID { get; set; }
        public string CourseTitle { get; set; }
        public string CourseLevel { get; set; }
        public int CoursePicture { get; set; }
        public int[] CourseTopics { get; set; }
        public int[] CourseExcercises { get; set; }
        public int[] CourseResources { get; set; }
        public int[] CourseExams { get; set; }

        public override string ToString()
        {
            return string.Format("[Course: CourseTitle={0}, CourseLevel={1}, CoursePicture={2}]", CourseTitle,
                CourseLevel, CoursePicture);
        }
    }
}