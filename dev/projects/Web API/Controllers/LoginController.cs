﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class LoginController : ApiController
    {
        
        Asura_InstituteEntities db = new Asura_InstituteEntities();

         public HttpResponseMessage PostRegister([FromBody] dynamic data)
        {

            string username = data.GetValue("username").ToString();
            string password = data.GetValue("password").ToString();

            Login login = new Login();
            login.Username = username;
            login.Password = password;
            db.Logins.Add(login);
            db.SaveChanges();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, "Registro exitoso de" + login.Username + " " + login.Password);

            return response;
        }


        public HttpResponseMessage GetUser(string username, string password)
        {
            var user = db.Logins.Where(x => x.Username == username && x.Password == password).FirstOrDefault();
            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Please Enter valid UserName and Password");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, "Success");
            }
        }




        /*
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
        */
    }
}
