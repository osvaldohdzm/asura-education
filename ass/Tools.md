# Información general de proyecto
El flujo de trabajo en proyectos es el expuesto en este documento.

### Logo creator

https://www.graphicsprings.com/

### Color palette

https://color.adobe.com/es/
https://www.materialpalette.com/

### Icon Launcher

https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html#foreground.type=image&foreground.space.trim=1&foreground.space.pad=0.1&foreColor=rgba(96%2C%20125%2C%20139%2C%200)&backColor=rgb(158%2C%205%2C%2069)&crop=0&backgroundShape=circle&effects=elevate&name=ic_launcher

##Vectorize
https://www.vectorizer.io/


https://materialdesignicons.com/